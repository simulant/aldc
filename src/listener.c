
#include "private.h"

void _alInitListener(Listener* listener) {
    _alInitTransform(&listener->transform);
    _alInitVec3(&listener->right);
    _alInitVec3(&listener->up);
    _alInitVec3(&listener->forward);

    listener->right.x = 1.0f;
    listener->up.y = 1.0f;
    listener->forward.z = -1.0f;
}

Listener* _alListener() {
    static Listener listener;
    return &listener;
}

AL_API void AL_APIENTRY alListenerf(ALenum param, ALfloat value) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alListener3f(ALenum param, ALfloat value1, ALfloat value2, ALfloat value3) {
    const ALenum VALID [] = {AL_POSITION, AL_VELOCITY, 0};

    if(!_alIsValidEnum(VALID, param)) {
        _alSetError(__func__, AL_INVALID_ENUM, "Invalid enum");
        return;
    }

    Listener* obj = _alListener();
    if(obj) {
        switch(param) {
            case AL_POSITION:
                obj->transform.position.x = value1;
                obj->transform.position.y = value2;
                obj->transform.position.z = value3;
                return;
            break;
            case AL_VELOCITY:
                obj->transform.velocity.x = value1;
                obj->transform.velocity.y = value2;
                obj->transform.velocity.z = value3;
                return;
            break;
            default:
                break;
        }
    }

    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alListenerfv(ALenum param, const ALfloat *values) {
    const ALenum VALID [] = {AL_POSITION, AL_VELOCITY, AL_ORIENTATION, 0};
    if(!_alIsValidEnum(VALID, param)) {
        _alSetError(__func__, AL_INVALID_ENUM, "Invalid enum");
        return;
    }

    Listener* obj = _alListener();

    switch(param) {
        case AL_POSITION:
            obj->transform.position.x = values[0];
            obj->transform.position.y = values[1];
            obj->transform.position.z = values[2];
            return;
        break;
        case AL_VELOCITY:
            obj->transform.velocity.x = values[0];
            obj->transform.velocity.y = values[1];
            obj->transform.velocity.z = values[2];
            return;
        break;
        case AL_ORIENTATION:
            obj->forward.x = values[0];
            obj->forward.y = values[1];
            obj->forward.z = values[2];
            obj->up.x = values[3];
            obj->up.y = values[4];
            obj->up.z = values[5];

            /* FIXME: Set `right`. */
            return;
        break;
        default:
            break;
    }

    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alListeneri(ALenum param, ALint value) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alListener3i(ALenum param, ALint value1, ALint value2, ALint value3) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alListeneriv(ALenum param, const ALint *values) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetListenerf(ALenum param, ALfloat *value) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetListener3f(ALenum param, ALfloat *value1, ALfloat *value2, ALfloat *value3) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetListenerfv(ALenum param, ALfloat *values) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetListeneri(ALenum param, ALint *value) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetListener3i(ALenum param, ALint *value1, ALint *value2, ALint *value3) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetListeneriv(ALenum param, ALint *values) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}
