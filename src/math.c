#include <math.h>

#include "math.h"

#ifdef _arch_dreamcast
#include <dc/vec3f.h>
#endif

void _alInitVec3(Vec3* v) {
    v->x = v->y = v->z = 0.0f;
}

float _alVec3Dot(const Vec3* lhs, const Vec3* rhs) {
#ifdef _arch_dreamcast
    float r;
    vec3f_dot(lhs->x, lhs->y, lhs->z, rhs->x, rhs->y, rhs->z, r);
    return r;
#else
    return lhs->x * rhs->x + lhs->y * rhs->y + lhs->z * rhs->z;
#endif
}

void _alVec3Subtract(const Vec3* lhs, const Vec3* rhs, Vec3* out) {
    out->x = lhs->x - rhs->x;
    out->y = lhs->y - rhs->y;
    out->z = lhs->z - rhs->z;
}

void _alVec3Normalize(Vec3* v) {
#ifdef _arch_dreamcast
    vec3f_normalize(v->x, v->y, v->z);
#else
    float l = 1.0f / _alVec3Length(v);

    v->x *= l;
    v->y *= l;
    v->z *= l;
#endif
}

float _alVec3Length(const Vec3* v) {
#ifdef _arch_dreamcast
    float r;
    vec3f_length(v->x, v->y, v->z, r);
    return r;
#else
    return sqrtf(v->x * v->x + v->y * v->y + v->z * v->z);
#endif
}

void _alVec3Cross(Vec3* out, const Vec3* a, const Vec3* b) {
    out->x = (a->y * b->z) - (a->z * b->y);
    out->y = (a->z * b->x) - (a->x * b->z);
    out->z = (a->x * b->y) - (a->y * b->x);
}
