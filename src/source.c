#include <float.h>
#include <stdio.h>

#include "private.h"

#define QUEUE_DEBUG 0


static ALint _alMarkAllBuffersProcessed(Source* source) {
    /* Set the processed count to the queued buffer count */
    source->buffers_processed = source_queue_size(source);
    return source->buffers_processed;
}

void _alInitBufferQueueIterator(BufferQueueIterator* iterator) {
    iterator->queue_index = -1;
    iterator->last_sample = 0;

    iterator->channels[0] = -1;
    iterator->channels[1] = -1;

    iterator->pan = 0.0f;
    iterator->gain = 1.0f;
}

void _alInitBufferQueueEntry(BufferQueueEntry* entry, Buffer* buffer) {
    entry->buffer = buffer;
}

void _alInitSource(Source* source) {
    _alInitTransform(&source->transform);

    source->name = 0;
    source->is_dead = false;
    source->reference_distance = 1.0f;
    source->rolloff_factor = 1.0f;
    source->max_distance = FLT_MAX;
    source->gain = 1.0f;
    source->pitch = 1.0f;
    source->state = AL_INITIAL;
    source->type = AL_UNDETERMINED;
    source->looping = AL_FALSE;
    source->relative = AL_FALSE;

    source->buffers_processed = 0;
    source->buffer_queue_front = 0;
    source->buffer_queue_back = 0;

    _alInitBufferQueueIterator(&source->buffer_queue_iterator);

    pthread_mutex_init(&source->mutex, NULL);
}

static void _alSourceResetQueue(Source* source) {
    memset(source->buffer_queue, 0, sizeof(source->buffer_queue));
    source->buffer_queue_front = source->buffer_queue_back = 0;
}

Source* _alSource(ALuint source) {
    Context* ctx = _alContext();
    if(!ctx) {
        return NULL;
    }

    assert(source > 0);
    assert(source < MAX_SOURCES_PER_CONTEXT);

    do {
        SCOPED_LOCK(&ctx->mutex);
        return _alSourceUnlocked(source);
    } while(0);
}

Source* _alSourceUnlocked(ALuint source) {
    Context* ctx = _alContext();
    Source* s = &ctx->sources[source - 1];
    if(s->is_dead) {
        return NULL;
    }

    return s;
}

AL_API void AL_APIENTRY alGenSources(ALsizei n, ALuint *sources) {
    Context* ctx = _alContext();
    if(!ctx) {
        _alSetError(__func__, AL_INVALID_OPERATION, "No current context");
        return;
    }

    ALsizei available = MAX_SOURCES_PER_CONTEXT - ctx->source_count;
    if(n > available) {
        _alSetError(__func__, AL_OUT_OF_MEMORY, "Not enough sources");
        return;
    }

    /* We need to lock here as we're updating the number of sources */
    SCOPED_LOCK(&ctx->mutex);

    Source* s = ctx->sources;
    ALsizei c = 0;
    for(int i = 0; i < MAX_SOURCES_PER_CONTEXT; ++i) {
        if(s[i].is_dead) {
            _alInitSource(s + i);
            sources[c++] = s[i].name = i + 1;

            /* If we have a queue, then something wasn't cleared
                * properly */
            assert(source_queue_empty(s + i));

            if(c == n) {
                break;
            }
        }
    }

    assert(c == n);
}

AL_API void AL_APIENTRY alDeleteSources(ALsizei n, const ALuint *sources) {
    Context* ctx = _alContext();
    if(!ctx) {
        _alSetError(__func__, AL_INVALID_OPERATION, "No current context");
        return;
    }

    SCOPED_LOCK(&ctx->mutex);

    for(int i = 0; i < n; ++i) {
        Source* s = _alSourceUnlocked(sources[i]);

        if(!s) {
            _alSetError(__func__, AL_INVALID_NAME, "Tried to delete an invalid source");
            continue;
        }

        do {
            SCOPED_LOCK(&s[i].mutex);

            /* Stop the source and mark all the buffers processed and unqueue them */
            _alSourceStopUnlocked(s);

            ALint processed = s->buffers_processed;
            _alSourceUnqueueBuffersUnlocked(s, processed, NULL);

            s->is_dead = true;
            s->name = 0;
            _alIteratorReleaseChannels(&s->buffer_queue_iterator);
        } while(0);

        pthread_mutex_destroy(&s->mutex);

        assert(source_queue_empty(s));

        assert(s->buffer_queue_iterator.queue_index == -1);
        assert(s->buffer_queue_iterator.channels[0] == -1);
        assert(s->buffer_queue_iterator.channels[1] == -1);

        assert(s->buffers_processed == 0);
    }
}

AL_API ALboolean AL_APIENTRY alIsSource(ALuint source) {
    return (_alSource(source)) ? AL_FALSE : AL_TRUE;
}

AL_API void AL_APIENTRY alSourcef(ALuint source, ALenum param, ALfloat value) {
    const ALenum VALID [] = {
        AL_GAIN,
        AL_ROLLOFF_FACTOR,
        AL_REFERENCE_DISTANCE,
        AL_PITCH,
        AL_MAX_DISTANCE,
        0
    };

    if(!_alIsValidEnum(VALID, param)) {
        _alSetError(__func__, AL_INVALID_ENUM, "Invalid enum");
        return;
    }

    Source* obj = _alSource(source);

    SCOPED_LOCK(&obj->mutex);

    if(obj) {
        switch(param) {
            case AL_PITCH:
                obj->pitch = value;
                return;
            case AL_GAIN:
                obj->gain = value;
                return;
            case AL_REFERENCE_DISTANCE:
                obj->reference_distance = value;
                return;
            case AL_ROLLOFF_FACTOR:
                obj->rolloff_factor = value;
                return;
            case AL_MAX_DISTANCE:
                obj->max_distance = value;
                return;
            default:
                break;
        }
    }

    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSource3f(ALuint source, ALenum param, ALfloat value1, ALfloat value2, ALfloat value3) {
    const ALenum VALID [] = {AL_POSITION, AL_VELOCITY, 0};

    if(!_alIsValidEnum(VALID, param)) {
        _alSetError(__func__, AL_INVALID_ENUM, "Invalid enum");
        return;
    }

    Source* obj = _alSource(source);

    if(obj) {
        switch(param) {
            case AL_POSITION:
                obj->transform.position.x = value1;
                obj->transform.position.y = value2;
                obj->transform.position.z = value3;
                return;
            break;
            case AL_VELOCITY:
                obj->transform.velocity.x = value1;
                obj->transform.velocity.y = value2;
                obj->transform.velocity.z = value3;
                return;
            break;
            default:
                break;
        }
    }

    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourcefv(ALuint source, ALenum param, const ALfloat *values) {
    const ALenum VALID [] = {AL_POSITION, AL_VELOCITY, 0};

    if(!_alIsValidEnum(VALID, param)) {
        _alSetError(__func__, AL_INVALID_ENUM, "Invalid enum");
        return;
    }

    Source* obj = _alSource(source);

    if(obj) {
        switch(param) {
            case AL_POSITION:
                obj->transform.position.x = values[0];
                obj->transform.position.y = values[1];
                obj->transform.position.z = values[2];
                return;
            break;
            case AL_VELOCITY:
                obj->transform.velocity.x = values[0];
                obj->transform.velocity.y = values[1];
                obj->transform.velocity.z = values[2];
                return;
            break;
            default:
                break;
        }
    }

    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourcei(ALuint source, ALenum param, ALint value) {

    Source* obj = _alSource(source);
    if(!obj) {
        _alSetError(__func__, AL_INVALID_NAME, "Invalid source name");
        return;
    }

    if(param == AL_LOOPING) {
        obj->looping = (value) ? AL_TRUE : AL_FALSE;

        /* If we set looping to true, we no longer report buffers
        as being processed - they're effectively always pending for
        replaying */
        if(obj->looping) {
            obj->buffers_processed = 0;
        }
        return;
    } else if(param == AL_BUFFER) {
        assert(obj);

        if(obj->state == AL_PLAYING || obj->state == AL_PAUSED) {
            _alSetError(__func__, AL_INVALID_OPERATION, "Tried to set the buffer on a playing source");
            return;
        }

        _alSourceResetQueue(obj);
        alSourceQueueBuffers(obj->name, 1, &value);
        obj->buffers_processed = 0;
        obj->buffer_queue_iterator.queue_index = 0;
        obj->type = (value) ? AL_STATIC : AL_UNDETERMINED;
        return;
    } else if(param == AL_SOURCE_RELATIVE) {
        obj->relative = (value) ? true : false;
        return;
    }

    _alSetError(__func__, AL_INVALID_OPERATION, "Not implemented");
}

AL_API void AL_APIENTRY alSource3i(ALuint source, ALenum param, ALint value1, ALint value2, ALint value3) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourceiv(ALuint source, ALenum param, const ALint *values) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetSourcef(ALuint source, ALenum param, ALfloat *value) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetSource3f(ALuint source, ALenum param, ALfloat *value1, ALfloat *value2, ALfloat *value3) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetSourcefv(ALuint source, ALenum param, ALfloat *values) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetSourcei(ALuint source,  ALenum param, ALint *value) {
    const ALenum VALID [] = {AL_SOURCE_STATE, AL_BUFFERS_PROCESSED, 0};
    if(!_alIsValidEnum(VALID, param)) {
        _alSetError(__func__, AL_INVALID_ENUM, "Invalid enum");
        return;
    }

    Source* obj = _alSource(source);
    if(obj) {
        SCOPED_LOCK(&obj->mutex);

        switch(param) {
            case AL_SOURCE_STATE:
                *value = (ALint) obj->state;
                return;
            break;
            case AL_BUFFERS_PROCESSED:
                /* We don't return the processed buffers if we're looping */
                *value = (obj->looping) ? 0 : obj->buffers_processed;
                return;
            break;
            default:
                _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
        }
    } else {
        _alSetError(__func__, AL_INVALID_NAME, "Couldn't find source");
        Context* ctx = _alContext();
        for(int i = 0; i < MAX_SOURCES_PER_CONTEXT; ++i) {
            if(!ctx->sources[i].is_dead) {
                printf("%d", ctx->sources[i].name);
            }
            if(i < MAX_BUFFERS_PER_CONTEXT - 1) {
                printf(", ");
            }
        }
        printf("\n");
    }
}

AL_API void AL_APIENTRY alGetSource3i(ALuint source, ALenum param, ALint *value1, ALint *value2, ALint *value3) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alGetSourceiv(ALuint source,  ALenum param, ALint *values) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourcePlayv(ALsizei n, const ALuint *sources) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourceStopv(ALsizei n, const ALuint *sources) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourceRewindv(ALsizei n, const ALuint *sources) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourcePausev(ALsizei n, const ALuint *sources) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourcePlay(ALuint source) {
    Source* obj = _alSource(source);
    if(obj) {
        SCOPED_LOCK(&obj->mutex);

        /* If we were paused, don't mess with whereever we were in the
        buffer queue */
        if(obj->state != AL_PAUSED) {
            obj->buffer_queue_iterator.queue_index = source_queue_empty(obj) ? -1 : 0;
            obj->buffers_processed = 0;
        }

        obj->state = AL_PLAYING;
    }
}

void _alSourceStopUnlocked(Source* obj) {
    if(obj->state == AL_INITIAL) {
        return;
    }

    obj->state = AL_STOPPED;
    obj->buffer_queue_iterator.queue_index = -1;

    _alMarkAllBuffersProcessed(obj);
}

AL_API void AL_APIENTRY alSourceStop(ALuint source) {
    Source* obj = _alSource(source);
    if(obj) {
        SCOPED_LOCK(&obj->mutex);
        _alSourceStopUnlocked(obj);
    }
}

AL_API void AL_APIENTRY alSourceRewind(ALuint source) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alSourcePause(ALuint source) {
    Source* obj = _alSource(source);
    if(obj) {
        SCOPED_LOCK(&obj->mutex);
        obj->state = AL_PAUSED;
    }
}

AL_API void AL_APIENTRY alSourceQueueBuffers(ALuint source, ALsizei nb, const ALuint *buffers) {
    Context* ctx = _alContext();
    Source* obj = _alSource(source);
    if(obj) {
        SCOPED_LOCK(&obj->mutex);

        for(ALsizei i = 0; i < nb; ++i) {
            BufferQueueEntry entry;

            if(buffers[i] == 0) {
                // The NULL buffer is a thing
                entry.buffer = &ctx->null_buffer;
            } else {
                entry.buffer = _alBuffer(buffers[i]);
                assert(entry.buffer);

                if(!entry.buffer) {
                    _alSetError(__func__, AL_INVALID_NAME, "Tried to queue an invalid or deleted buffer");
                    return;
                }

                if(!source_queue_empty(obj)) {
                    assert(entry.buffer);

                    if(source_queue_front(obj)->buffer->format != entry.buffer->format) {
                        _alSetError(__func__, AL_INVALID_OPERATION, "Format mismatch when queuing buffers");
                        return;
                    }
                }
            }

            if(!source_queue_push(obj, &entry)) {
                _alSetError(__func__, AL_OUT_OF_MEMORY, "Ran out of run in the buffer queue");
                return;
            }
        }

#if QUEUE_DEBUG
        printf("Queued %d buffers tp %x\n", nb, obj);
        printf("New queue: ");
        for(int i = 0; i < source_queue_size(obj); ++i) {
            printf("%x (%d) ", source_queue_at(obj, i), source_queue_at(obj, i)->buffer->name);
        }
        printf("\n");
#endif
    } else {
        _alSetError(__func__, AL_INVALID_NAME, "Invalid source specified");
    }
}



AL_API void AL_APIENTRY alSourceUnqueueBuffers(ALuint source, ALsizei nb, ALuint *buffers) {
    Source* obj = _alSource(source);

    if(obj) {
        SCOPED_LOCK(&obj->mutex);
        return _alSourceUnqueueBuffersUnlocked(obj, nb, buffers);
    } else {
        _alSetError(__func__, AL_INVALID_NAME, "Invalid source specified");
        return;
    }

}

AL_API void AL_APIENTRY _alSourceUnqueueBuffersUnlocked(Source* obj, ALsizei nb, ALuint *buffers) {
    if(nb > obj->buffers_processed) {
        _alSetError(__func__, AL_INVALID_VALUE, "Requested to unqueue more buffers than processed");
        return;
    }

    for(ALsizei i = 0; i < nb; ++i) {
        BufferQueueEntry* entry = source_queue_front(obj);
        assert(entry);

        /* FIXME: Handle case where the current entry is unqueued while
        * it's being played */
        assert(obj->buffer_queue_iterator.queue_index != 0);

        /* Get the buffer that the head is pointing at */
        if(buffers) {
            buffers[i] = entry->buffer->name;
        }

        /* Shift the head of the queue */
        if(!source_queue_pop(obj)) {
            _alSetError(__func__, AL_INVALID_OPERATION, "Failed to unqueue the buffer!");
            buffers[i] = 0;
            return;
        }

        assert(obj->buffers_processed > 0);
        obj->buffers_processed--;

        /* If we shrink the queue, we also need to shrink the queue index
        * because whatever we were indexing before will now be 1 closer
        * to the front. */
        if(obj->buffer_queue_iterator.queue_index > -1) {
            obj->buffer_queue_iterator.queue_index--;
        }

        /* If we cleared the buffer queue, then the iterator is done */
        if(source_queue_empty(obj)) {
            obj->buffer_queue_iterator.queue_index = -1;
        }
    }

#if QUEUE_DEBUG
    printf("Unqueued %d buffers from %x\n", nb, obj);
    printf("New queue: ");
    for(int i = 0; i < source_queue_size(obj); ++i) {
        printf("%x (%d) ", source_queue_at(obj, i), source_queue_at(obj, i)->buffer->name);
    }
    printf("\n");
#endif

}
