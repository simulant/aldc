#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include "platform.h"

#ifdef _arch_dreamcast

#include "aica_cmd_iface.h"

#define SPU_RAM_BASE            0xa0800000


#define SPU_CHANNEL_BASE        0xA0700000
#define SPU_CHANNEL_SIZE        32 * 4

#define dc_snd_base ((volatile unsigned char *) SPU_CHANNEL_BASE)

#define SNDREG32A(x) ((volatile unsigned long *)(dc_snd_base + (x)))
#define SNDREG32(x) (*SNDREG32A(x))
#define SNDREG8A(x) (dc_snd_base + (x))
#define SNDREG8(x) (*SNDREG8A(x))

void init_spu() {
    snd_init();
    snd_sh4_to_aica_start();
}

bool use_time_based_buffer_switching() {
    return true;
}

bool channel_done(int channel) {
    // NOT USED
    return false;
}

void play_channel(int channel, int addr, int freq, int bits, int size, float vol, float pan) {
    AICA_CMDSTR_CHANNEL(tmp, cmd, chan);

    if(size >= 65535) {
        size = 65534;
    }

    cmd->cmd = AICA_CMD_CHAN;
    cmd->timestamp = 0;
    cmd->size = AICA_CMDSTR_CHANNEL_SIZE;
    cmd->cmd_id = channel;
    chan->cmd = AICA_CH_CMD_START;
    chan->base = addr;
    chan->type = (bits == 16) ? AICA_SM_16BIT : AICA_SM_8BIT;
    chan->length = size;
    chan->loop = 0;
    chan->loopstart = 0;
    chan->loopend = size;
    chan->freq = freq;

    chan->vol = 255.0f * vol;
    if(chan->vol > 255) { chan->vol = 255;}

    chan->pan = (128.0f * pan) + 128.0f;
    if(chan->pan > 255) chan->pan = 255;

    snd_sh4_to_aica(tmp, cmd->size);
}

void play_channels(int c0, int c1, int addr0, int addr1, int freq, int bits, int size, float vol) {
    AICA_CMDSTR_CHANNEL(tmp, cmd, chan);

    if(size >= 65535) size = 65534;

    cmd->cmd = AICA_CMD_CHAN;
    cmd->timestamp = 0;
    cmd->size = AICA_CMDSTR_CHANNEL_SIZE;
    cmd->cmd_id = c0;
    chan->cmd = AICA_CH_CMD_START | AICA_CH_START_DELAY;
    chan->base = addr0;
    chan->type = (bits == 16) ? AICA_SM_16BIT : AICA_SM_8BIT;
    chan->length = size;
    chan->loop = 0;
    chan->loopstart = 0;
    chan->loopend = size;
    chan->freq = freq;
    chan->vol = 255.0f * vol;

    if(chan->vol > 255) {
        chan->vol = 255;
    }

    chan->pan = 0;
    snd_sh4_to_aica(tmp, cmd->size);

    cmd->cmd_id = c1;
    chan->base = addr1;
    chan->pan = 255;
    snd_sh4_to_aica(tmp, cmd->size);

    cmd->cmd_id = (1 << c0) | (1 << c1);
    chan->cmd = AICA_CH_CMD_START | AICA_CH_START_SYNC;
    snd_sh4_to_aica(tmp, cmd->size);
}

void stop_channel(int channel) {
    AICA_CMDSTR_CHANNEL(tmp, cmd, chan);

    cmd->cmd = AICA_CMD_CHAN;
    cmd->timestamp = 0;
    cmd->size = AICA_CMDSTR_CHANNEL_SIZE;
    cmd->cmd_id = channel;
    chan->cmd = AICA_CH_CMD_STOP;
    snd_sh4_to_aica(tmp, cmd->size);
}

int channel_position(int channel) {
    assert(channel > -1);

    return g2_read_32(SPU_RAM_BASE + AICA_CHANNEL(channel) + offsetof(aica_channel_t, pos));
}

void pan_channel(int channel, float pan) {
    assert(channel >= 0);

    if(channel < 0) return;

    int final_pan = (2.0f - (pan + 1.0f)) * 128.0f;
    final_pan = (final_pan > 255) ? 255 : final_pan;
    final_pan = (final_pan < 0) ? 0 : final_pan;

    AICA_CMDSTR_CHANNEL(tmp, cmd, chan);

    cmd->cmd = AICA_CMD_CHAN;
    cmd->timestamp = 0;
    cmd->size = AICA_CMDSTR_CHANNEL_SIZE;
    cmd->cmd_id = channel;
    chan->cmd = AICA_CH_CMD_UPDATE | AICA_CH_UPDATE_SET_PAN;
    chan->pan = 255 - final_pan;  /* FIXME: Figure out why this is inverse? */
    snd_sh4_to_aica(tmp, cmd->size);
}

void amp_channel(int channel, float v) {
    assert(channel >= 0);

    if(channel < 0) return;

    AICA_CMDSTR_CHANNEL(tmp, cmd, chan);

    /* Clamp */
    v = (v > 1.0f) ? 1.0f : (v < 0.0f) ? 0.0f : v;

    cmd->cmd = AICA_CMD_CHAN;
    cmd->timestamp = 0;
    cmd->size = AICA_CMDSTR_CHANNEL_SIZE;
    cmd->cmd_id = channel;
    chan->cmd = AICA_CH_CMD_UPDATE | AICA_CH_UPDATE_SET_VOL;
    chan->vol = 255.0f * v;
    snd_sh4_to_aica(tmp, cmd->size);
}

void pitch_channel(int channel, int new_freq) {
    /* FIXME: Disabled for now! */
    return;

    if(channel < 0) return;

    AICA_CMDSTR_CHANNEL(tmp, cmd, chan);

    cmd->cmd = AICA_CMD_CHAN;
    cmd->timestamp = 0;
    cmd->size = AICA_CMDSTR_CHANNEL_SIZE;
    cmd->cmd_id = channel;
    chan->cmd = AICA_CH_CMD_UPDATE | AICA_CH_UPDATE_SET_FREQ;
    chan->freq = new_freq;
    snd_sh4_to_aica(tmp, cmd->size);
}

#elif defined(__PSP__)

#include <pspthreadman.h>
#include <pspaudio.h>

#define SAMPLES_PER_CHANNEL 32768

typedef struct {
    float gain;
    float pan;
    int samples;
} ChanInfo;

static ChanInfo chan_info[PSP_AUDIO_CHANNEL_MAX] = {0};

bool use_time_based_buffer_switching() {
    return true;
}

bool channel_done(int channel) {
    return sceAudioGetChannelRestLength(channel) == 0;
}

int alloc_spu_ram(int size) {
    // Potentially over-allocate
    intptr_t ptr = (intptr_t) malloc(PSP_AUDIO_SAMPLE_ALIGN(size));
    return (int) ptr;
}

void free_spu_ram(int addr) {
    free((void*) addr);
}

void copy_to_spu(int32_t addr, void* src, int size) {
    void* dest = (void*) addr;
    memcpy(dest, src, size);
}

int alloc_spu_channel() {
    int channel = sceAudioChReserve(-1, SAMPLES_PER_CHANNEL, PSP_AUDIO_FORMAT_MONO);

    // Reset the volume
    if(channel >= 0 && channel < PSP_AUDIO_CHANNEL_MAX) {
        chan_info[channel].gain = 1.0f;
        chan_info[channel].pan = 0.0f;
    }

    return channel;
}

void release_spu_channel(int c) {
    if(c < 0) {
        return;
    }
    sceAudioChRelease(c);
}

void init_spu() {

}

void play_channel(int channel, int addr, int freq, int bits, int size, float gain, float pan) {
    if(channel < 0) {
        return;
    }

    chan_info[channel].gain = 1.0f;
    chan_info[channel].pan = 0.0f;

    float left = gain * (1.0 - ((pan + 1) * 0.5f));
    float right = gain * ((pan + 1) * 0.5f);

    // Align the size, but zero out any extra data
    int aligned = PSP_AUDIO_SAMPLE_ALIGN(size);
    if(aligned > size) {
        void* off = ((uint8_t*)addr) + size;
        memset(off, 0, aligned - size);
    }

    // FIXME: this alignment will round *up* ... I hope this doesn't overflow
    // something
    chan_info[channel].samples = aligned;
    sceAudioSetChannelDataLen(channel, aligned);

    sceAudioOutputPanned(channel, left * 32768, right * 32768, (void*)addr);
}

void play_channels(int c0, int c1, int addr0, int addr1, int freq, int bits, int size, float gain) {
    play_channel(c0, addr0, freq, bits, size, gain, -1.0);
    play_channel(c1, addr1, freq, bits, size, gain, 1.0);
}

void stop_channel(int chan) {
    if(chan < 0) {
        return;
    }

    int remaining = sceAudioGetChannelRestLength(chan);
    int sent = chan_info[chan].samples;
    int processed = sent - remaining;
    chan_info[chan].samples = processed;
    sceAudioSetChannelDataLen(chan, processed);
}

void pan_channel(int channel, float v) {
    if(channel < 0) {
        return;
    }

    float left = chan_info[channel].gain * (1.0 - ((v + 1) * 0.5f));
    float right = chan_info[channel].gain * ((v + 1) * 0.5f);

    sceAudioChangeChannelVolume(channel, left * 32768, right * 32768);
}

void amp_channel(int channel, float v) {
    chan_info[channel].gain = (v < 0) ? 0 : (v > 1.0f) ? 1.0f : v;
    pan_channel(channel, chan_info[channel].pan);
}

void pitch_channel(int channel, int new_freq) {

}

int channel_position(int channel) {
    return chan_info[channel].samples - sceAudioGetChannelRestLength(channel);
}

uint64_t time_in_ms() {
    return sceKernelGetSystemTimeWide() / 1000;
}

#else

#include <sys/time.h>

/* Really stupid emulation of what the DC is doing, except the
 * DC is actually doing some sensible memory allocation rather than
 * this! */

#define CHUNK_COUNT 1024
static uint8_t* CHUNKS[CHUNK_COUNT] = {0};

int alloc_spu_ram(int size) {
    for(int i = 0; i < 1024; ++i) {
        if(!CHUNKS[i]) {
            CHUNKS[i] = (uint8_t*) malloc(size);
            return i + 1;
        }
    }

    /* Out of "memory"! */
    return 0;
}

void free_spu_ram(int addr) {
    free(CHUNKS[addr - 1]);
    CHUNKS[addr - 1] = NULL;
}

void copy_to_spu(int32_t addr, void* src, int size) {
    memcpy(CHUNKS[addr - 1], src, size);
}

void init_spu() {

}

static uint64_t channels = 0;

int alloc_spu_channel() {
    int i = 0;
    while((channels & (1 << i))) {
        if(++i == 64) {
            fprintf(stderr, "[ALdc] Channel overflow\n");
            return -1;
        }
    }

    channels |= (1 << i);
    return i;
}

void release_spu_channel(int c) {
    channels &= ~(1 << c);
}

void play_channel(int channel, int addr, int freq, int bits, int size, float gain, float pan) {

}

void play_channels(int c0, int c1, int addr0, int addr1, int freq, int bits, int size, float gain) {

}

int channel_position(int channel) {
    return 0;
}

void stop_channel(int channel) {

}

void pan_channel(int channel, float pan) {

}

void amp_channel(int channel, float v) {

}

void pitch_channel(int channel, int freq) {}

uint64_t time_in_ms() {
    struct timeval time;
    gettimeofday(&time, NULL);
    int64_t s1 = (int64_t)(time.tv_sec) * 1000;
    int64_t s2 = (time.tv_usec / 1000);
    return s1 + s2;
}

bool use_time_based_buffer_switching() {
    return true;
}

bool channel_done(int channel) {
    // NOT USED
    return false;
}


#endif
